from typing import List


# https://leetcode.cn/problems/maximum-subarray/solutions/2361770/53-zui-da-zi-shu-zu-he-dong-tai-gui-hua-bvkq9/
class Solution:
    # 最大子数组和
    def maxSubArray(self, nums: List[int]) -> int:
        for i in range(1, len(nums)):
            nums[i] += max(nums[i - 1], 0)
        return max(nums)


""" https://leetcode.cn/problems/maximum-product-subarray/solutions/17709/duo-chong-si-lu-qiu-jie-by-powcai-3/
dynamic plan 最大值和前面的值有关，三种情况：
    当前值可能是最大值
    当前值*前面的最大值
    当前值*前面的最小值
"""
class Solution1:
    def maxProduct(self, nums: List[int]) -> int:
        reverseNums = nums[::-1]
        for i, _ in enumerate(nums[1:], start=1):
            nums[i] *= nums[i - 1] or 1
            reverseNums[i] *= reverseNums[i - 1] or 1
        return max(reverseNums + nums)

"""
https://leetcode.cn/problems/subsets/description/
输入：nums = [1,2,3]
输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
"""
class Solution3:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        res = [[]]
        for num in nums:
            res = res + [i + [num] for i in res]
        return res
