from typing import List

from copy import deepcopy
from collections import defaultdict

class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        sumDicIndex = defaultdict(list)
        for i, candidate in enumerate(candidates):
            self.updateSumDicIndex(sumDicIndex, candidate, i)  # 先更新旧的，然后添加新的
            sumDicIndex[candidate].append(i)
        return sumDicIndex[target]

    def updateSumDicIndex(self, sumDicIndexDic: dict, candidate, index):
        copySumDicIndex = deepcopy(sumDicIndexDic)
        for sumKey, i in sumDicIndexDic.items():
            copySumDicIndex[sumKey + candidate].append(i + [index])
        sumDicIndexDic = copySumDicIndex

candidates = [2,3,6,7]
target = 7
print(Solution().combinationSum(candidates,target))  # [[2,2,3],[7]]

candidates = [2,3,5]
target = 8
print(Solution().combinationSum(candidates,target))  # [[2,2,2,2],[2,3,3],[3,5]]

candidates = [2]
target = 1
print(Solution().combinationSum(candidates,target))  # []
